# -*- coding: utf-8 -*-

import json
import time

class Config:
	def load(self, filename):
		with open(filename) as f:
			config = json.load(f)

		now = time.gmtime()
		updates = []

		for fmt in config.keys():
			if not fmt.endswith("_format"): continue
			updates.append((fmt[:-7], time.strftime(config[fmt], now)))

		config.update(updates)

		self.config = config
		return self

	def __getitem__(self, key):
		return self.config[key]

	__getattr__ = __getitem__
