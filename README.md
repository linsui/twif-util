# twif-util
Helper tools for managing TWIF (This Week In F-Droid)

## What is this?
A collection of user-unfriendly helpers for managing TWIF (This Week In F-Droid)

## Why does it exist?
* It is difficult to get a list of new and/or updated apps. No such list is published anywhere. So we pull it straight out of the index in a format that is hopefully diff-friendly.
* No sane person should be tasked with manually linkifying all the apps. That's what this does, too.

## Status
**Hack**. (Not user friendly. Not clean code. No documentation. Very unfinished sausage.)

## Recommended Installation
```
$ cd $SOMEWHERE
$ git clone https://gitlab.com/Matrixcoffee/twif-util.git
$ mkdir twif-util/untracked
$ cp config.json untracked/
$ vi untracked/config.json
```
Should your Python lack socks support, you can add it to your project like so:
```
$ wget -P twif-util https://github.com/Anorov/PySocks/raw/master/socks.py
```

## Reference Docs
* https://f-droid.org/en/docs/Build_Metadata_Reference/
* https://f-droid.org/en/docs/All_About_Descriptions_Graphics_and_Screenshots
* https://gitlab.com/fdroid/fdroidserver/issues/387

## License
Copyright 2018 `@Coffee:matrix.org`

   > Licensed under the Apache License, Version 2.0 (the "License");
   > you may not use this file except in compliance with the License.

   > The full text of the License can be obtained from the file called [LICENSE](LICENSE).

   > Unless required by applicable law or agreed to in writing, software
   > distributed under the License is distributed on an "AS IS" BASIS,
   > WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   > See the License for the specific language governing permissions and
   > limitations under the License.
