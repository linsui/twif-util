#!/bin/bash

set -o noclobber
shopt -s nullglob
export LC_ALL=C.UTF-8

for LANGPATH in ../../fdroid-website/_posts{,/[a-z][a-z]{,_*,[a-z]}}; do
	LANG="${LANGPATH##*/}"
	[ "$LANG" = "_posts" ] && LANG=en
	#echo "$LANG"

	for F in "$LANGPATH"/*.md; do
		#echo "$F"
		B="${F##*/}"
		T="../test/hugo/content/posts/${B%.md}.$LANG.md"
		if [ ! -e "$T" -o "$F" -nt "$T" -o convert_to_new.py -nt "$T" -o convert_to_new.bash -nt "$T" ]; then
			echo "$T"

			rm -vf "$T"
			if python3 convert_to_new.py "$F" > "$T"; then
				echo Success
			else
				echo Failed
				rm -v "$T"
				sleep 30
			fi

			rm test
			python3 convert_to_new.py "$F" > test || exit 1
			read -rn 1 -t 0.1 && colordiff -u "$F" test | less -R
			[ "$REPLY" == "q" ] && exit 0
		fi
	done
done
