#!/bin/bash

### Safe deduplication script
###
### - Move (not REmove) detected duplicates to .dup
### - Full comparison, no hash
### - Duplicates are assumed to be sequential in the argument list (non-consecutive dups are not detected)
### - Later files are assumed to be duplicates of files appearing earlier in the argument list

eecho ()
{
	echo "$@" 1>&2
}

failed ()
{
	eecho "Failed" "$@"
	exit 1
}

try ()
{
	eecho "> $*"
	"$@" || failed to execute \""$@"\"
}

AWK_GET_MD5='
	BEGIN {
		for (x=0; x < 32; x++) p = p "[0-9a-f]"
		p = "\\." p "\\."
	}
	{
		if (match($0, p)) print substr($0, RSTART+1, RLENGTH-2)
	}
'

FILES=( "$@" )

for (( i="$#"; i > 1; i-- )); do
	P="${FILES[$i - 2]}"	# Previous
	F="${FILES[$i - 1]}"	# Current
	D="$F".dup		# Duplicate

	if cmp -s "$P" "$F"; then
		eecho "\"$P\" and \"$F\" are equal: moving to \"$D"
		try mv -v "$F" "$D"
	fi
done
