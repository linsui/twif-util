# stdlib
import argparse
import collections
import reprlib

def parse_args():
	parser = argparse.ArgumentParser(description="diff_index.py compares two indexes and spits out hopefully interesting information.")
	parser.add_argument("-d", "--debug", action='store_true', default=False, help="Enable debug messages.")
	parser.add_argument("old")
	parser.add_argument("new")
	options = parser.parse_args()
	return options

class _Missing:
	@staticmethod
	def __repr__():
		return "!Missing!"

Missing = _Missing()

class Stats:
	class SimpleAddedRemoved:
		def __init__(self):
			self.added = 0
			self.removed = 0

		def add(self):
			self.added += 1

		def remove(self):
			self.removed += 1

		def _v(self):
			return (('added', 'removed'), (self.added, self.removed))

		def __repr__(self):
			return _mkrepr(self.__class__.__name__, *self._v())

	class AddedRemoved:
		def __init__(self):
			self.added = collections.Counter()
			self.removed = collections.Counter()

		def add(self, item):
			self.added[item] += 1

		def remove(self, item):
			self.removed[item] += 1

		def _v(self):
			return (('added', 'removed'), (self.added.items(), self.removed.items()))

		def __repr__(self):
			return _mkrepr(self.__class__.__name__, *self._v())

		def print(self, indent="", indent2=None):
			if indent2 is None: indent2 = indent + " "

			for h, l in zip(*self._v()):
				print(indent, h.capitalize(), ":", sep="")
				for thing, count in l:
					print(indent2, thing, ": ", count, sep="")

	class AddedRemovedChanged(AddedRemoved):
		def __init__(self):
			super().__init__()
			self.changed = collections.Counter()

		def change(self, item):
			self.changed[item] += 1

		def _v(self):
			return (('added', 'removed', 'changed'), (self.added.items(), self.removed.items(), self.changed.items()))

	def __init__(self):
		self.apps = self.SimpleAddedRemoved()
		self.app_keys = self.AddedRemovedChanged()
		self.languages = self.SimpleAddedRemoved()
		self.localized_keys = self.AddedRemovedChanged()

		self.missing_app_key_new = collections.Counter()

		self.total = 0

	def print_stats(self):
		print("Total items processed:", self.total)
		print("Removed Apps:      {:3} ({:5.1%})".format(*self._p(self.apps.removed)))
		print("Added Apps:        {:3} ({:5.1%})".format(*self._p(self.apps.added)))
		print("Removed Languages: {:3} ({:5.1%})".format(*self._p(self.languages.removed)))
		print("Added Languages:   {:3} ({:5.1%})".format(*self._p(self.languages.added)))
		print("Keys:")
		self.app_keys.print(indent=" ")
		print("Localized Keys:")
		self.localized_keys.print(indent=" ")
		print(repr(self.__dict__))

	def _p(self, value):
		return (value, value / self.total)

xrepr = reprlib.Repr()
xrepr.maxstring=60

def _mkrepr(classname, names, values, limit=1000):
	r = []
	skipped = False
	for i, n, v in zip(range(len(names)), names, values):
		if v is None:
			skipped = True
			continue
		if skipped or i >= limit: r.append("{}={}".format(n, xrepr.repr(v)))
		else: r.append(xrepr.repr(v))

	return "{}({})".format(classname, ", ".join(r))

class DiffResult:
	class AddedRemoved:
		def __init__(self):
			self.added = collections.deque()
			self.removed = collections.deque()

		def add(self, item):
			return self.added.append(item)

		def remove(self, item):
			return self.removed.append(item)

		def _v(self):
			return (('added', 'removed'), (self.added, self.removed))

		def __repr__(self):
			return _mkrepr(self.__class__.__name__, *self._v())

		def print(self, indent="", indent2=None):
			if indent2 is None: indent2 = indent + " "

			for h, l in zip(*self._v()):
				if not l: continue
				print(indent, h.capitalize(), ":", sep="")
				for thing in l:
					print(indent2, thing, sep="")

	class AddedRemovedChanged(AddedRemoved):
		def __init__(self):
			super().__init__()
			self.changed = collections.deque()

		def change(self, item):
			return self.changed.append(item)

		def _v(self):
			return (('added', 'removed', 'changed'), (self.added, self.removed, self.changed))

	class Item:
		Changed = collections.namedtuple("Changed", ('old', 'new'))

		def __init__(self, app_id, key=None, language=None, old=None, new=None):
			self.app_id = app_id
			self.key = key
			self.language = language
			self.value = self.Changed(old, new)

		def __repr__(self):
			return _mkrepr(self.__class__.__name__, *self._v())

		def _v(self):
			return ("app_id key language old new".split(),
				(self.app_id, self.key, self.language, self.value.old, self.value.new))

	def __init__(self):
		self.apps = self.AddedRemoved()
		self.app_keys = self.AddedRemovedChanged()
		self.languages = self.AddedRemoved()
		self.localized_keys = self.AddedRemovedChanged()

	def _v(self):
		return ("apps app_keys languages localized_keys".split(),
			(self.apps, self.app_keys, self.languages, self.localized_keys))

	def print_results(self):
		for n, v in zip(*self._v()):
			print(n.capitalize(), ":", sep="")
			v.print(indent=" ")

def get_unified_apps(oldindex, newindex):
	""" Return the union of oldindex['apps'] and newindex['apps']

	as a sorted list of (app_id, oldindex['apps'][app_id], newindex['apps'][app_id]) tuples. """

	oldapps = oldindex.idx['apps']
	newapps = newindex.idx['apps']

	keyset = set(oldapps.keys())
	keyset.update(newapps.keys())

	allkeys = []
	for key in sorted(keyset):
		allkeys.append((key, oldapps.get(key, Missing), newapps.get(key, Missing)))

	return allkeys

def get_app_key_with_lang(app, key, fallback_langs=("en_US",)):
	""" Try really hard to get a key from an app entry.

	If 'key' is not in the main entry, try 'fallback_langs' in order, and if that doesn't work,
	try to get it from _any_ language at all in no particular order.

	Return value: A tuple of language and value.
	Language is None if the key was found on the main entry.
	Returns (None, Missing) if nothing found.
	"""

	try: return (None, app[key])
	except KeyError: pass

	try: localized = app['localized']
	except KeyError: return (None, Missing)

	for lang in fallback_langs:
		try: return (lang, localized['lang'][key])
		except KeyError: pass

	for lang, langobj in localized.items():
		try: return (lang, langobj[key])
		except KeyError: pass

	return (None, Missing)

def get_app_key(*args, **kwargs):
	""" Try really hard to get a key from an app entry.

	If 'key' is not in the main entry, try 'fallback_langs' in order, and if that doesn't work,
	try to get it from _any_ language at all in no particular order.

	Return value: Value, or Missing if not found.
	"""

	return get_app_key_with_lang(*args, **kwargs)[1]

if __name__ == '__main__':
	opts = parse_args()
	print(repr(opts))

	from Index import Index
	oldindex = Index().load_from_file(opts.old)
	newindex = Index().load_from_file(opts.new)
	allapps = get_unified_apps(oldindex, newindex)

	stats = Stats()
	stats.total = len(allapps)

	diffresult = DiffResult()
	diffresult.total = len(allapps)

	for pkgid, oldapp, newapp in allapps:
		if newapp is Missing:
			print("Removed:", repr(pkgid))
			diffresult.apps.remove(diffresult.Item(pkgid))
			stats.apps.remove()

	for pkgid, oldapp, newapp in allapps:
		if oldapp is Missing:
			print("Added:", repr(pkgid))
			diffresult.apps.add(diffresult.Item(pkgid))
			stats.apps.add()

	for pkgid, oldapp, newapp in allapps:
		# Diffing things, so both must be present
		if oldapp is Missing or newapp is Missing: continue

		for key, value in oldapp.items():
			if key not in newapp:
				print("Removed key: {!r}->{!r}".format(pkgid, key))
				diffresult.app_keys.remove(diffresult.Item(pkgid, key))
				stats.app_keys.remove(key)
			elif value != newapp[key] and key not in ('localized', 'packages'):
				print("Changed value: {!r}->{!r} {!r}->{!r}".format(pkgid, key, value, newapp[key]))
				diffresult.app_keys.change(diffresult.Item(pkgid, key, old=value, new=newapp[key]))
				stats.app_keys.change(key)

		for key in newapp.keys():
			if key not in oldapp:
				print("Added key: {!r}->{!r}".format(pkgid, key))
				diffresult.app_keys.add(diffresult.Item(pkgid, key))
				stats.app_keys.add(key)

		oldlocalized = oldapp.get('localized', Missing)
		newlocalized = newapp.get('localized', Missing)

		if oldlocalized is not Missing and newlocalized is not Missing:
			for lang, langdata in oldlocalized.items():
				if lang not in newlocalized:
					print("Removed language: {!r} lang {!r}".format(pkgid, lang))
					diffresult.languages.remove(diffresult.Item(pkgid, language=lang))
					stats.languages.remove()
				else:
					for key, value in langdata.items():
						if key not in newlocalized[lang]:
							print("Removed key: {!r}->{!r}->{!r}->{!r}".format(pkgid, 'localized', lang, key))
							diffresult.localized_keys.remove(diffresult.Item(pkgid, key, lang))
							stats.localized_keys.remove(key)
						elif value != newlocalized[lang][key]:
							diffresult.localized_keys.change(diffresult.Item(pkgid, key, lang, old=value, new=newlocalized[lang][key]))
							stats.localized_keys.change(key)
							print("Changed value: {!r}->{!r}->{!r}->{!r} {!r}->{!r}".format(pkgid, 'localized', lang, key, value, newlocalized[lang][key]))

			for lang, langdata in newlocalized.items():
				if lang not in oldlocalized:
					print("Added language: {!r} lang {!r}".format(pkgid, lang))
					diffresult.languages.add(diffresult.Item(pkgid, language=lang))
					stats.languages.add()
				else:
					for key, value in langdata.items():
						if key not in oldlocalized[lang]:
							print("Added key: {!r}->{!r}->{!r}->{!r}".format(pkgid, 'localized', lang, key))
							diffresult.localized_keys.add(diffresult.Item(pkgid, key, lang))
							stats.localized_keys.add(key)

	diffresult.print_results()

	stats.print_stats()
