# stdlib
import argparse
import collections
import reprlib

def parse_args():
	parser = argparse.ArgumentParser(description="lint_index.py parses and analizes an index file and spits out hopefully interesting information.")
	parser.add_argument("-d", "--debug", action='store_true', default=False, help="Enable debug messages.")
	parser.add_argument("index")
	options = parser.parse_args()
	return options

class _Missing:
	@staticmethod
	def __repr__():
		return "!Missing!"

Missing = _Missing()

class ReprAble:
	def _v(self):
		return ((), ())

	def __repr__(self):
		return _mkrepr(self.__class__.__name__, *self._v())

class Stats:
	class Warning(ReprAble):
		def __init__(self, appid, lang, key, errcode, txt):
			self.appid = appid
			self.lang = lang
			self.key = key
			self.errcode = errcode
			self.txt = txt

		def _v(self):
			return (('appid', 'lang', 'key', 'errcode', 'txt'), (self.appid, self.lang, self.key, self.errcode, self.txt))

	class Error(Warning):
		pass

	def __init__(self):
		self.missing_app_key = collections.Counter()

		self.warnings = collections.defaultdict(list)
		self.warningsByType = collections.Counter()
		self.errors = collections.defaultdict(list)
		self.errorsByType = collections.Counter()

		self.total = 0

	def warning(self, appid, lang, key, errcode, txt):
		w = self.Warning(appid, lang, key, errcode, txt)
		self.warnings[appid].append(w)
		self.warningsByType[errcode] += 1
		print(repr(w))

	def error(self, appid, lang, key, errcode, txt):
		e = self.Error(appid, lang, key, errcode, txt)
		self.errors[appid].append(e)
		self.errorsByType[errcode] += 1
		print(repr(e))

	def print_stats(self):
		allapps = set(self.warnings.keys())
		allapps.update(self.errors.keys())

		totalwarnings = 0
		totalerrors = 0

		for appid in sorted(allapps):
			ww = len(self.warnings[appid])
			totalwarnings += ww
			www = tuple(x.errcode for x in self.warnings[appid])
			ee = len(self.errors[appid])
			totalerrors += ee
			eee = tuple(x.errcode for x in self.errors[appid])
			if ee: xx = "!!"
			else:  xx = "  "
			#print("{}{}: {} warnings {} errors".format(xx, appid, ww, ee))
			print("{}{}: {!r} {!r}".format(xx, appid, www, eee))

		#print("Removed Apps:      {:3} ({:5.1%})".format(*self._p(self.apps.removed)))
		#print("Added Apps:        {:3} ({:5.1%})".format(*self._p(self.apps.added)))
		#print("Removed Languages: {:3} ({:5.1%})".format(*self._p(self.languages.removed)))
		#print("Added Languages:   {:3} ({:5.1%})".format(*self._p(self.languages.added)))
		#print("Keys:")
		#self.app_keys.print(indent=" ")
		#print("Localized Keys:")
		#self.localized_keys.print(indent=" ")
		#print(repr(self.__dict__))

		for www, ww in self.warningsByType.most_common():
			print("{:20} {:3} ({:5.1%})".format(www, *self._p(ww)))

		print("{} {:3} ({:5.1%})".format("Total Warnings:", *self._p(totalwarnings)))

		for eee, ee in self.errorsByType.most_common():
			print("{:20} {:3} ({:5.1%})".format(eee, *self._p(ee)))

		print("{} {:3} ({:5.1%})".format("Total Errors:", *self._p(totalerrors)))

		#print("Warnings:          {:3} ({:5.1%})".format(*self._p(totalwarnings)))
		#print("Errors:            {:3} ({:5.1%})".format(*self._p(totalerrors)))

		print("Total {} apps tested for {} conditions".format(self.total, self.totaltests))

	def _p(self, value):
		return (value, value / self.total)

xrepr = reprlib.Repr()
xrepr.maxstring=70

def _mkrepr(classname, names, values, limit=1000):
	r = []
	skipped = False
	for i, n, v in zip(range(len(names)), names, values):
		if v is None:
			skipped = True
			continue
		if skipped or i >= limit: r.append("{}={}".format(n, repr(v)))
		else: r.append(repr(v))

	return "{}({})".format(classname, ", ".join(r))

class Required(str):
	pass

Required.Top = Required('Top')		# Required and must be at toplevel
Required.Any = Required('Any')		# Required, can be at toplevel or localized
Required.No = Required('No')		# Optional
Required.NotTop = Required('NotTop')	# Optional, must not appear at toplevel (non-spec)
Required.Ignore = Required('Ignore')	# Alias for No, but signals that it isn't specced

Multiline = "Multiline"

app_keys = {
	'added': (int, Required.Top, {}),
	'antiFeatures': (list, Required.No, {}),
	'authorEmail': (str, Required.No, {}),
	'authorName': (str, Required.No, {}),
	'authorWebSite': (str, Required.Ignore, {}),
	'binaries': (str, Required.No, {}),
	'bitcoin': (str, Required.No, {}),
	'categories': (list, Required.Top, {}),
	'changelog': (str, Required.No, {}),
	'description': (str, Required.Any, {Multiline}),
	'donate': (str, Required.No, {}),
	'featureGraphic': (str, Required.NotTop, {}),
	'flattrID': (str, Required.No, {}),
	'icon': (str, Required.Any, {}),
	'issueTracker': (str, Required.No, {}),
	'lastUpdated': (int, Required.Top, {}),
	'liberapayID': (str, Required.No, {}),
	'license': (str, Required.Top, {}),
	'litecoin': (str, Required.No, {}),
	'localized': (dict, Required.No, {}),
	'name': (str, Required.Any, {}),
	'packageName': (str, Required.Top, {}),
	'packages': (list, Required.Top, {}),
	'phoneScreenshots': (list, Required.NotTop, {}),
	'promoGraphic': (str, Required.NotTop, {}),
	'sevenInchScreenshots': (list, Required.NotTop, {}),
	'sourceCode': (str, Required.No, {}),
	'suggestedVersionCode': (str, Required.Top, {}),
	'suggestedVersionName': (str, Required.Top, {}),
	'summary': (str, Required.Any, {}),
	'tenInchScreenshots': (list, Required.NotTop, {}),
	'tvBanner': (str, Required.NotTop, {}),
	'tvScreenshots': (list, Required.NotTop, {}),
	'video': (str, Required.NotTop, {}),
	'wearScreenshots': (list, Required.NotTop, {}),
	'webSite': (str, Required.No, {}),
	'whatsNew': (str, Required.NotTop, {Multiline})
}

app_keys_sorted = list(sorted(app_keys.items()))

package_keys = {
	'added': (int, Required.Top, {}),
	'apkName': (str, Required.Top, {}),
	'hash': (str, Required.Top, {}),
	'hashType': (str, Required.Top, {}),
	'minSdkVersion': (str, Required.Top, {}),
	'packageName': (str, Required.Top, {}),
	'sig': (str, Required.Top, {}),
	'signer': (str, Required.Top, {}),
	'size': (int, Required.Top, {}),
	'srcname': (str, Required.Top, {}),
	'targetSdkVersion': (str, Required.Top, {}),
	'uses-permission': (list, Required.Top, {}),
	'versionCode': (int, Required.Top, {}),
	'versionName': (str, Required.Top, {})
}

package_keys_sorted = list(sorted(package_keys.items()))

def get_app_key_with_lang(app, key, fallback_langs=("en_US",)):
	""" Try really hard to get a key from an app entry.

	If 'key' is not in the main entry, try 'fallback_langs' in order, and if that doesn't work,
	try to get it from _any_ language at all in no particular order.

	Return value: A tuple of language and value.
	Language is None if the key was found on the main entry.
	Returns (None, Missing) if nothing found.
	"""

	try: return (None, app[key])
	except KeyError: pass

	try: localized = app['localized']
	except KeyError: return (None, Missing)

	for lang in fallback_langs:
		try: return (lang, localized['lang'][key])
		except KeyError: pass

	for lang, langobj in localized.items():
		try: return (lang, langobj[key])
		except KeyError: pass

	return (None, Missing)

def get_app_key(*args, **kwargs):
	""" Try really hard to get a key from an app entry.

	If 'key' is not in the main entry, try 'fallback_langs' in order, and if that doesn't work,
	try to get it from _any_ language at all in no particular order.

	Return value: Value, or Missing if not found.
	"""

	return get_app_key_with_lang(*args, **kwargs)[1]

def is_missing_app_key(app, key, req):
	""" Return True if key _should_ be present in app but isn't, False otherwise """

	if not isinstance(req, Required): raise Exception("Bad type for req")

	if req == Required.No: return False
	if req == Required.Any: return get_app_key(app, key) is Missing
	if req == Required.Top: return key not in app
	if req == Required.NotTop: return False
	if req == Required.Ignore: return False

	raise Exception("Bad value for req")

def _le(lang, error):
	if lang is None: return error
	return "Localized" + error


class Tests:
	def __init__(self, index):
		self.index = index
		self.stats = Stats()
		self.apps = tuple(sorted(index.idx['apps'].items()))
		self.stats.total = len(self.apps)

	def sanity_check_value(self, appid, app, lang, key, value, template=app_keys):
		try:
			ytpe, required, flags = template[key]
		except KeyError:
			return

		if not isinstance(value, ytpe):
			self.stats.error(appid, lang, key, _le(lang, 'TypeError'), "Value has type {!r} instead of {!r}".format(type(value), ytpe))
			return

		if not isinstance(value, str): return

		if Multiline in flags:
			if value != value.lstrip():
				self.stats.error(appid, lang, key, _le(lang, 'LeadingTrimError'), "Value contains leading whitespace")
			if value != value.rstrip():
				self.stats.warning(appid, lang, key, _le(lang, 'TrailingTrimWarning'), "Value contains trailing whitespace")

		else:
			if "\n" in value:
				self.stats.error(appid, lang, key, _le(lang, 'MultilineError'), "Illegal newline in value") 
			if value != value.lstrip():
				self.stats.error(appid, lang, key, _le(lang, 'LeadingTrimError'), "Value contains leading whitespace")
			if value != value.rstrip():
				self.stats.error(appid, lang, key, _le(lang, 'TrailingTrimError'), "Value contains trailing whitespace")

	def test_required_keys(self, appid, app):
		for key, (ytpe, required, flags) in app_keys_sorted:
			if is_missing_app_key(app, key, required):
				#self.stats.missing_app_key[key] += 1
				#print("Missing: {!r}->{!r}".format(pkgid, key))
				self.stats.error(appid, None, key, 'MissingKeyError', "Key is required but missing")

	def test_unknown_keys(self, appid, app):
		for key in app.keys():
			if app_keys.get(key, (None, Required.NotTop))[1] == Required.NotTop:
				#print("Extra key (new): {!r}->{!r}".format(pkgid, key))
				self.stats.error(appid, None, key, 'UnknownKeyError', "Key is not in the list of known keys")

	def test_localized_unknown_keys(self, appid, app):
		localized = app.get('localized', {})
		for lang, langdata in localized.items():
			for key in langdata.keys():
				if key not in app_keys:
					self.stats.error(appid, lang, key, 'UnknownKeyError', "Key is not in the list of known keys")
					#print("Extra key (new): {!r}->{!r}->{!r}->{!r}".format(pkgid, 'localized', lang, key))

	def test_root_values(self, appid, app):
		''' Several general sanity checks on root values '''
		for key, value in app.items():
			self.sanity_check_value(appid, app, None, key, value)

	def test_localized_values(self, appid, app):
		''' Several general sanity checks on localized values '''
		localized = app.get('localized', {})
		for lang, langdata in localized.items():
			for key, value in langdata.items():
				self.sanity_check_value(appid, app, lang, key, value)

	def test_package_values(self, appid, app):
		''' Several general sanity checks on package values '''
		for package in app['packages']:
			for key, value in package.items():
				self.sanity_check_value(appid, app, None, key, value, package_keys)

	def test_suggestedVersionCode_type(self, appid, app):
		svcs = app['suggestedVersionCode']
		svc = int(svcs)
		if str(svc) != svcs:
			self.stats.error(appid, None, 'suggestedVersionCode', 'ValueError', "{!r} is not a canonical integer".format(svcs))

	def test_suggestedVersionCode_held(self, appid, app):
		svc = int(app['suggestedVersionCode'])
		vcs = index.get_available_version_codes(app)
		if svc in vcs and svc != vcs[-1]:
			self.stats.warning(appid, None, 'suggestedVersionCode', 'HeldWarning', "Versions {!r} available but held at {!r}".format(vcs, svc))

	def test_suggestedVersionCode_nobuilds(self, appid, app):
		vcs = index.get_available_version_codes(app)
		if len(vcs) == 0:
			self.stats.error(appid, None, 'packages', 'NoBuildsError', "No builds available!")

	def test_suggestedVersionCode_missing_low(self, appid, app):
		svc = int(app['suggestedVersionCode'])
		vcs = index.get_available_version_codes(app)
		if vcs and svc not in vcs and svc < vcs[0]:
			self.stats.error(appid, None, 'suggestedVersionCode', 'MissingBuildError', "All available builds {!r} are newer than {!r}".format(vcs, svc))

	def test_suggestedVersionCode_missing_high(self, appid, app):
		svc = int(app['suggestedVersionCode'])
		vcs = index.get_available_version_codes(app)
		if vcs and svc not in vcs and svc > vcs[0]:
			self.stats.warning(appid, None, 'suggestedVersionCode', 'MissingBuildWarning', "Version {!r} is newer than available builds {!r}".format(svc, vcs))

	def test_name_equals_en_US_name(self, appid, app):
		try:
			name = app['name']
			lname = app['localized']['en-US']['name']
			if name != lname:
				self.stats.warning(appid, None, 'name', 'NameLanguageWarning', "Localized en-US name {!r} differs from root {!r}".format(lname, name))
		except KeyError:
			pass

	def _get_tests(self):
		return tuple(getattr(self, x) for x in dir(self) if x.startswith("test_"))

	def run_all_tests_by_app(self):
		print(repr(self.__dict__.keys()))
		tests = self._get_tests()
		self.stats.totaltests = len(tests)
		for appid, app in self.apps:
			for test in tests:
				test(appid, app)

	def run_all_tests_by_test(self):
		tests = self._get_tests()
		self.stats.totaltests = len(tests)
		for test in tests:
			for appid, app in self.apps:
				test(appid, app)

if __name__ == '__main__':
	opts = parse_args()
	print(repr(opts))

	from Index import Index
	index = Index().load_from_file(opts.index)

	#stats = Stats()
	#stats.total = len(index.idx['apps'])
	#apps = tuple(sorted(index.idx['apps'].items()))

	tests = Tests(index)
	tests.run_all_tests_by_test()

	tests.stats.print_stats()
