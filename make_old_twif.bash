#!/bin/bash

SINCE="last thursday"
[ -n "$1" ] && SINCE="$1"

exec python3 make_old_twif.py "$( date -d "$SINCE" +'../../index/index-v1-%Y-%m-%d-%a.jar' )"
